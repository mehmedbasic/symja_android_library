package org.matheclipse.core.integrate.rubi45;


import static org.matheclipse.core.expression.F.*;
import static org.matheclipse.core.integrate.rubi45.UtilityFunctionCtors.*;
import static org.matheclipse.core.integrate.rubi45.UtilityFunctions.*;
import org.matheclipse.core.interfaces.IAST;
import org.matheclipse.core.interfaces.IExpr;
import org.matheclipse.core.interfaces.ISymbol;

/** 
 * IndefiniteIntegrationRules from the <a href="http://www.apmaths.uwo.ca/~arich/">Rubi -
 * rule-based integrator</a>.
 *  
 */
public class IntRules97 { 
  public static IAST RULES = List( 
ISetDelayed(Int(Power(Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_DEFAULT),x_Symbol),
    Condition(Module(List(Set(g,Numerator(Power(pn,CN1)))),Times(g,Subst(Int(Times(Power(x,Plus(g,Times(CN1,C1))),Power(Sinh(Plus(a,Times(b,Power(x,Times(pn,g))))),p)),x),x,Power(x,Power(g,CN1))))),And(And(FreeQ(List(a,b,p),x),RationalQ(pn)),Or(Less(pn,C0),FractionQ(pn))))),
ISetDelayed(Int(Power(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_DEFAULT),x_Symbol),
    Condition(Module(List(Set(g,Numerator(Power(pn,CN1)))),Times(g,Subst(Int(Times(Power(x,Plus(g,Times(CN1,C1))),Power(Cosh(Plus(a,Times(b,Power(x,Times(pn,g))))),p)),x),x,Power(x,Power(g,CN1))))),And(And(FreeQ(List(a,b,p),x),RationalQ(pn)),Or(Less(pn,C0),FractionQ(pn))))),
ISetDelayed(Int(Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),x_Symbol),
    Condition(Plus(Times(C1D2,Int(Power(E,Plus(a,Times(b,Power(x,pn)))),x)),Times(CN1,C1D2,Int(Power(E,Plus(Times(CN1,a),Times(CN1,b,Power(x,pn)))),x))),FreeQ(List(a,b,pn),x))),
ISetDelayed(Int(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),x_Symbol),
    Condition(Plus(Times(C1D2,Int(Power(E,Plus(a,Times(b,Power(x,pn)))),x)),Times(C1D2,Int(Power(E,Plus(Times(CN1,a),Times(CN1,b,Power(x,pn)))),x))),FreeQ(List(a,b,pn),x))),
ISetDelayed(Int(Power(Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_),x_Symbol),
    Condition(Int(ExpandTrigReduce(Power(Sinh(Plus(a,Times(b,Power(x,pn)))),p),x),x),And(And(FreeQ(List(a,b,pn),x),IntegerQ(p)),Greater(p,C1)))),
ISetDelayed(Int(Power(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_),x_Symbol),
    Condition(Int(ExpandTrigReduce(Power(Cosh(Plus(a,Times(b,Power(x,pn)))),p),x),x),And(And(FreeQ(List(a,b,pn),x),IntegerQ(p)),Greater(p,C1)))),
ISetDelayed(Int(Power(Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_DEFAULT),x_Symbol),
    Condition($(Defer($s("Int")),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),p),x),FreeQ(List(a,b,pn,p),x))),
ISetDelayed(Int(Power(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_DEFAULT),x_Symbol),
    Condition($(Defer($s("Int")),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),p),x),FreeQ(List(a,b,pn,p),x))),
ISetDelayed(Int(Power(Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(u_,pn_)))),p_DEFAULT),x_Symbol),
    Condition(Times(Power(Coefficient(u,x,C1),CN1),Subst(Int(Power(Sinh(Plus(a,Times(b,Power(x,pn)))),p),x),x,u)),And(And(FreeQ(List(a,b,pn,p),x),LinearQ(u,x)),NonzeroQ(Plus(u,Times(CN1,x)))))),
ISetDelayed(Int(Power(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(u_,pn_)))),p_DEFAULT),x_Symbol),
    Condition(Times(Power(Coefficient(u,x,C1),CN1),Subst(Int(Power(Cosh(Plus(a,Times(b,Power(x,pn)))),p),x),x,u)),And(And(FreeQ(List(a,b,pn,p),x),LinearQ(u,x)),NonzeroQ(Plus(u,Times(CN1,x)))))),
ISetDelayed(Int(Times(Power(x_,CN1),Sinh(Times(b_DEFAULT,Power(x_,pn_)))),x_Symbol),
    Condition(Times(SinhIntegral(Times(b,Power(x,pn))),Power(pn,CN1)),FreeQ(List(b,pn),x))),
ISetDelayed(Int(Times(Cosh(Times(b_DEFAULT,Power(x_,pn_))),Power(x_,CN1)),x_Symbol),
    Condition(Times(CoshIntegral(Times(b,Power(x,pn))),Power(pn,CN1)),FreeQ(List(b,pn),x))),
ISetDelayed(Int(Times(Power(x_,CN1),Sinh(Plus(a_,Times(b_DEFAULT,Power(x_,pn_))))),x_Symbol),
    Condition(Plus(Times(Sinh(a),Int(Times(Cosh(Times(b,Power(x,pn))),Power(x,CN1)),x)),Times(Cosh(a),Int(Times(Sinh(Times(b,Power(x,pn))),Power(x,CN1)),x))),FreeQ(List(a,b,pn),x))),
ISetDelayed(Int(Times(Cosh(Plus(a_,Times(b_DEFAULT,Power(x_,pn_)))),Power(x_,CN1)),x_Symbol),
    Condition(Plus(Times(Cosh(a),Int(Times(Cosh(Times(b,Power(x,pn))),Power(x,CN1)),x)),Times(Sinh(a),Int(Times(Sinh(Times(b,Power(x,pn))),Power(x,CN1)),x))),FreeQ(List(a,b,pn),x))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_))))),x_Symbol),
    Condition(Times(Cosh(Plus(a,Times(b,Power(x,pn)))),Power(Times(b,pn),CN1)),And(FreeQ(List(a,b,m,pn),x),ZeroQ(Plus(m,Times(CN1,Plus(pn,Times(CN1,C1)))))))),
ISetDelayed(Int(Times(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Times(Sinh(Plus(a,Times(b,Power(x,pn)))),Power(Times(b,pn),CN1)),And(FreeQ(List(a,b,m,pn),x),ZeroQ(Plus(m,Times(CN1,Plus(pn,Times(CN1,C1)))))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_))))),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,Times(CN1,pn),C1)),Cosh(Plus(a,Times(b,Power(x,pn)))),Power(Times(b,pn),CN1)),Times(CN1,Plus(m,Times(CN1,pn),C1),Power(Times(b,pn),CN1),Int(Times(Power(x,Plus(m,Times(CN1,pn))),Cosh(Plus(a,Times(b,Power(x,pn))))),x))),And(And(FreeQ(List(a,b),x),RationalQ(m,pn)),Or(Less(Less(C0,pn),Plus(m,C1)),Less(Less(Plus(m,C1),pn),C0))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_))))),x_Symbol),
    Condition(Module(List(Set($s("mn"),Simplify(Plus(m,Times(CN1,pn))))),Plus(Times(Power(x,Plus($s("mn"),C1)),Cosh(Plus(a,Times(b,Power(x,pn)))),Power(Times(b,pn),CN1)),Times(CN1,Plus($s("mn"),C1),Power(Times(b,pn),CN1),Int(Times(Power(x,$s("mn")),Cosh(Plus(a,Times(b,Power(x,pn))))),x)))),And(And(FreeQ(List(a,b,m,pn),x),NonzeroQ(Plus(m,Times(CN1,pn),C1))),PositiveIntegerQ(Simplify(Times(Plus(m,C1),Power(pn,CN1))))))),
ISetDelayed(Int(Times(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,Times(CN1,pn),C1)),Sinh(Plus(a,Times(b,Power(x,pn)))),Power(Times(b,pn),CN1)),Times(CN1,Plus(m,Times(CN1,pn),C1),Power(Times(b,pn),CN1),Int(Times(Power(x,Plus(m,Times(CN1,pn))),Sinh(Plus(a,Times(b,Power(x,pn))))),x))),And(And(FreeQ(List(a,b),x),RationalQ(m,pn)),Or(Less(Less(C0,pn),Plus(m,C1)),Less(Less(Plus(m,C1),pn),C0))))),
ISetDelayed(Int(Times(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Module(List(Set($s("mn"),Simplify(Plus(m,Times(CN1,pn))))),Plus(Times(Power(x,Plus($s("mn"),C1)),Sinh(Plus(a,Times(b,Power(x,pn)))),Power(Times(b,pn),CN1)),Times(CN1,Plus($s("mn"),C1),Power(Times(b,pn),CN1),Int(Times(Power(x,$s("mn")),Sinh(Plus(a,Times(b,Power(x,pn))))),x)))),And(And(FreeQ(List(a,b,m,pn),x),NonzeroQ(Plus(m,Times(CN1,pn),C1))),PositiveIntegerQ(Simplify(Times(Plus(m,C1),Power(pn,CN1))))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_))))),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,C1)),Sinh(Plus(a,Times(b,Power(x,pn)))),Power(Plus(m,C1),CN1)),Times(CN1,b,pn,Power(Plus(m,C1),CN1),Int(Times(Power(x,Plus(m,pn)),Cosh(Plus(a,Times(b,Power(x,pn))))),x))),And(And(FreeQ(List(a,b),x),RationalQ(m,pn)),Or(And(Greater(pn,C0),Less(m,CN1)),And(Less(pn,C0),Greater(m,CN1)))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_))))),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,C1)),Sinh(Plus(a,Times(b,Power(x,pn)))),Power(Plus(m,C1),CN1)),Times(CN1,b,pn,Power(Plus(m,C1),CN1),Int(Times(Power(x,Simplify(Plus(m,pn))),Cosh(Plus(a,Times(b,Power(x,pn))))),x))),And(FreeQ(List(a,b,m,pn),x),NegativeIntegerQ(Simplify(Times(Plus(m,C1),Power(pn,CN1))))))),
ISetDelayed(Int(Times(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,C1)),Cosh(Plus(a,Times(b,Power(x,pn)))),Power(Plus(m,C1),CN1)),Times(CN1,b,pn,Power(Plus(m,C1),CN1),Int(Times(Power(x,Plus(m,pn)),Sinh(Plus(a,Times(b,Power(x,pn))))),x))),And(And(FreeQ(List(a,b),x),RationalQ(m,pn)),Or(And(Greater(pn,C0),Less(m,CN1)),And(Less(pn,C0),Greater(m,CN1)))))),
ISetDelayed(Int(Times(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,C1)),Cosh(Plus(a,Times(b,Power(x,pn)))),Power(Plus(m,C1),CN1)),Times(CN1,b,pn,Power(Plus(m,C1),CN1),Int(Times(Power(x,Simplify(Plus(m,pn))),Sinh(Plus(a,Times(b,Power(x,pn))))),x))),And(FreeQ(List(a,b,m,pn),x),NegativeIntegerQ(Simplify(Times(Plus(m,C1),Power(pn,CN1))))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_))))),x_Symbol),
    Condition(Plus(Times(C1D2,Int(Times(Power(x,m),Power(E,Plus(a,Times(b,Power(x,pn))))),x)),Times(CN1,C1D2,Int(Times(Power(x,m),Power(E,Plus(Times(CN1,a),Times(CN1,b,Power(x,pn))))),x))),FreeQ(List(a,b,m,pn),x))),
ISetDelayed(Int(Times(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),Power(x_,m_DEFAULT)),x_Symbol),
    Condition(Plus(Times(C1D2,Int(Times(Power(x,m),Power(E,Plus(a,Times(b,Power(x,pn))))),x)),Times(C1D2,Int(Times(Power(x,m),Power(E,Plus(Times(CN1,a),Times(CN1,b,Power(x,pn))))),x))),FreeQ(List(a,b,m,pn),x))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_)),x_Symbol),
    Condition(Plus(Times(CN1,Power(Sinh(Plus(a,Times(b,Power(x,pn)))),p),Power(Times(Plus(pn,Times(CN1,C1)),Power(x,Plus(pn,Times(CN1,C1)))),CN1)),Times(b,pn,p,Power(Plus(pn,Times(CN1,C1)),CN1),Int(Times(Power(Sinh(Plus(a,Times(b,Power(x,pn)))),Plus(p,Times(CN1,C1))),Cosh(Plus(a,Times(b,Power(x,pn))))),x))),And(And(And(And(FreeQ(List(a,b),x),IntegersQ(pn,p)),ZeroQ(Plus(m,pn))),Greater(p,C1)),NonzeroQ(Plus(pn,Times(CN1,C1)))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_)),x_Symbol),
    Condition(Plus(Times(CN1,Power(Cosh(Plus(a,Times(b,Power(x,pn)))),p),Power(Times(Plus(pn,Times(CN1,C1)),Power(x,Plus(pn,Times(CN1,C1)))),CN1)),Times(b,pn,p,Power(Plus(pn,Times(CN1,C1)),CN1),Int(Times(Power(Cosh(Plus(a,Times(b,Power(x,pn)))),Plus(p,Times(CN1,C1))),Sinh(Plus(a,Times(b,Power(x,pn))))),x))),And(And(And(And(FreeQ(List(a,b),x),IntegersQ(pn,p)),ZeroQ(Plus(m,pn))),Greater(p,C1)),NonzeroQ(Plus(pn,Times(CN1,C1)))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_)),x_Symbol),
    Condition(Plus(Times(CN1,pn,Power(Sinh(Plus(a,Times(b,Power(x,pn)))),p),Power(Times(Sqr(b),Sqr(pn),Sqr(p)),CN1)),Times(Power(x,pn),Cosh(Plus(a,Times(b,Power(x,pn)))),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),Plus(p,Times(CN1,C1))),Power(Times(b,pn,p),CN1)),Times(CN1,Plus(p,Times(CN1,C1)),Power(p,CN1),Int(Times(Power(x,m),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),Plus(p,Times(CN1,C2)))),x))),And(And(And(FreeQ(List(a,b,m,pn),x),ZeroQ(Plus(m,Times(CN1,C2,pn),C1))),RationalQ(p)),Greater(p,C1)))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_)),x_Symbol),
    Condition(Plus(Times(CN1,pn,Power(Cosh(Plus(a,Times(b,Power(x,pn)))),p),Power(Times(Sqr(b),Sqr(pn),Sqr(p)),CN1)),Times(Power(x,pn),Sinh(Plus(a,Times(b,Power(x,pn)))),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),Plus(p,Times(CN1,C1))),Power(Times(b,pn,p),CN1)),Times(Plus(p,Times(CN1,C1)),Power(p,CN1),Int(Times(Power(x,m),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),Plus(p,Times(CN1,C2)))),x))),And(And(And(FreeQ(List(a,b,m,pn),x),ZeroQ(Plus(m,Times(CN1,C2,pn),C1))),RationalQ(p)),Greater(p,C1)))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_)),x_Symbol),
    Condition(Plus(Times(CN1,Plus(m,Times(CN1,pn),C1),Power(x,Plus(m,Times(CN1,C2,pn),C1)),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),p),Power(Times(Sqr(b),Sqr(pn),Sqr(p)),CN1)),Times(Power(x,Plus(m,Times(CN1,pn),C1)),Cosh(Plus(a,Times(b,Power(x,pn)))),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),Plus(p,Times(CN1,C1))),Power(Times(b,pn,p),CN1)),Times(CN1,Plus(p,Times(CN1,C1)),Power(p,CN1),Int(Times(Power(x,m),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),Plus(p,Times(CN1,C2)))),x)),Times(Plus(m,Times(CN1,pn),C1),Plus(m,Times(CN1,C2,pn),C1),Power(Times(Sqr(b),Sqr(pn),Sqr(p)),CN1),Int(Times(Power(x,Plus(m,Times(CN1,C2,pn))),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),p)),x))),And(And(And(And(FreeQ(List(a,b),x),IntegersQ(m,pn)),RationalQ(p)),Greater(p,C1)),Less(Less(C0,Times(C2,pn)),Plus(m,C1))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_)),x_Symbol),
    Condition(Plus(Times(CN1,Plus(m,Times(CN1,pn),C1),Power(x,Plus(m,Times(CN1,C2,pn),C1)),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),p),Power(Times(Sqr(b),Sqr(pn),Sqr(p)),CN1)),Times(Power(x,Plus(m,Times(CN1,pn),C1)),Sinh(Plus(a,Times(b,Power(x,pn)))),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),Plus(p,Times(CN1,C1))),Power(Times(b,pn,p),CN1)),Times(Plus(p,Times(CN1,C1)),Power(p,CN1),Int(Times(Power(x,m),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),Plus(p,Times(CN1,C2)))),x)),Times(Plus(m,Times(CN1,pn),C1),Plus(m,Times(CN1,C2,pn),C1),Power(Times(Sqr(b),Sqr(pn),Sqr(p)),CN1),Int(Times(Power(x,Plus(m,Times(CN1,C2,pn))),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),p)),x))),And(And(And(And(FreeQ(List(a,b),x),IntegersQ(m,pn)),RationalQ(p)),Greater(p,C1)),Less(Less(C0,Times(C2,pn)),Plus(m,C1))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,C1)),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),p),Power(Plus(m,C1),CN1)),Times(CN1,b,pn,p,Power(x,Plus(m,pn,C1)),Cosh(Plus(a,Times(b,Power(x,pn)))),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),Plus(p,Times(CN1,C1))),Power(Times(Plus(m,C1),Plus(m,pn,C1)),CN1)),Times(Sqr(b),Sqr(pn),Sqr(p),Power(Times(Plus(m,C1),Plus(m,pn,C1)),CN1),Int(Times(Power(x,Plus(m,Times(C2,pn))),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),p)),x)),Times(Sqr(b),Sqr(pn),p,Plus(p,Times(CN1,C1)),Power(Times(Plus(m,C1),Plus(m,pn,C1)),CN1),Int(Times(Power(x,Plus(m,Times(C2,pn))),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),Plus(p,Times(CN1,C2)))),x))),And(And(And(And(And(FreeQ(List(a,b),x),IntegersQ(m,pn)),RationalQ(p)),Greater(p,C1)),Less(Less(C0,Times(C2,pn)),Plus(C1,Times(CN1,m)))),NonzeroQ(Plus(m,pn,C1))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,C1)),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),p),Power(Plus(m,C1),CN1)),Times(CN1,b,pn,p,Power(x,Plus(m,pn,C1)),Sinh(Plus(a,Times(b,Power(x,pn)))),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),Plus(p,Times(CN1,C1))),Power(Times(Plus(m,C1),Plus(m,pn,C1)),CN1)),Times(Sqr(b),Sqr(pn),Sqr(p),Power(Times(Plus(m,C1),Plus(m,pn,C1)),CN1),Int(Times(Power(x,Plus(m,Times(C2,pn))),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),p)),x)),Times(CN1,Sqr(b),Sqr(pn),p,Plus(p,Times(CN1,C1)),Power(Times(Plus(m,C1),Plus(m,pn,C1)),CN1),Int(Times(Power(x,Plus(m,Times(C2,pn))),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),Plus(p,Times(CN1,C2)))),x))),And(And(And(And(And(FreeQ(List(a,b),x),IntegersQ(m,pn)),RationalQ(p)),Greater(p,C1)),Less(Less(C0,Times(C2,pn)),Plus(C1,Times(CN1,m)))),NonzeroQ(Plus(m,pn,C1))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_)),x_Symbol),
    Condition(Times(Power(Plus(m,C1),CN1),Subst(Int(Power(Sinh(Plus(a,Times(b,Power(x,Simplify(Times(pn,Power(Plus(m,C1),CN1))))))),p),x),x,Power(x,Plus(m,C1)))),And(And(FreeQ(List(a,b,m,pn,p),x),NonzeroQ(Plus(m,C1))),PositiveIntegerQ(Simplify(Times(pn,Power(Plus(m,C1),CN1))))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_)),x_Symbol),
    Condition(Times(Power(Plus(m,C1),CN1),Subst(Int(Power(Cosh(Plus(a,Times(b,Power(x,Simplify(Times(pn,Power(Plus(m,C1),CN1))))))),p),x),x,Power(x,Plus(m,C1)))),And(And(FreeQ(List(a,b,m,pn,p),x),NonzeroQ(Plus(m,C1))),PositiveIntegerQ(Simplify(Times(pn,Power(Plus(m,C1),CN1))))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_)),x_Symbol),
    Condition(Int(ExpandTrigReduce(Power(x,m),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),p),x),x),And(And(And(And(FreeQ(List(a,b,m,pn),x),IntegerQ(p)),Greater(p,C1)),Not(FractionQ(m))),Not(FractionQ(pn))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_)),x_Symbol),
    Condition(Int(ExpandTrigReduce(Power(x,m),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),p),x),x),And(And(And(And(FreeQ(List(a,b,m,pn),x),IntegerQ(p)),Greater(p,C1)),Not(FractionQ(m))),Not(FractionQ(pn))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_)),x_Symbol),
    Condition(Plus(Times(Power(x,pn),Cosh(Plus(a,Times(b,Power(x,pn)))),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C1)),Power(Times(b,pn,Plus(p,C1)),CN1)),Times(CN1,pn,Power(Sinh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C2)),Power(Times(Sqr(b),Sqr(pn),Plus(p,C1),Plus(p,C2)),CN1)),Times(CN1,Plus(p,C2),Power(Plus(p,C1),CN1),Int(Times(Power(x,m),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C2))),x))),And(And(And(And(FreeQ(List(a,b,m,pn),x),ZeroQ(Plus(m,Times(CN1,C2,pn),C1))),RationalQ(p)),Less(p,CN1)),Unequal(p,CN2)))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_)),x_Symbol),
    Condition(Plus(Times(CN1,Power(x,pn),Sinh(Plus(a,Times(b,Power(x,pn)))),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C1)),Power(Times(b,pn,Plus(p,C1)),CN1)),Times(pn,Power(Cosh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C2)),Power(Times(Sqr(b),Sqr(pn),Plus(p,C1),Plus(p,C2)),CN1)),Times(Plus(p,C2),Power(Plus(p,C1),CN1),Int(Times(Power(x,m),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C2))),x))),And(And(And(And(FreeQ(List(a,b,m,pn),x),ZeroQ(Plus(m,Times(CN1,C2,pn),C1))),RationalQ(p)),Less(p,CN1)),Unequal(p,CN2)))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,Times(CN1,pn),C1)),Cosh(Plus(a,Times(b,Power(x,pn)))),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C1)),Power(Times(b,pn,Plus(p,C1)),CN1)),Times(CN1,Plus(m,Times(CN1,pn),C1),Power(x,Plus(m,Times(CN1,C2,pn),C1)),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C2)),Power(Times(Sqr(b),Sqr(pn),Plus(p,C1),Plus(p,C2)),CN1)),Times(CN1,Plus(p,C2),Power(Plus(p,C1),CN1),Int(Times(Power(x,m),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C2))),x)),Times(Plus(m,Times(CN1,pn),C1),Plus(m,Times(CN1,C2,pn),C1),Power(Times(Sqr(b),Sqr(pn),Plus(p,C1),Plus(p,C2)),CN1),Int(Times(Power(x,Plus(m,Times(CN1,C2,pn))),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C2))),x))),And(And(And(And(And(FreeQ(List(a,b),x),IntegersQ(m,pn)),RationalQ(p)),Less(p,CN1)),Unequal(p,CN2)),Less(Less(C0,Times(C2,pn)),Plus(m,C1))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_)),x_Symbol),
    Condition(Plus(Times(CN1,Power(x,Plus(m,Times(CN1,pn),C1)),Sinh(Plus(a,Times(b,Power(x,pn)))),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C1)),Power(Times(b,pn,Plus(p,C1)),CN1)),Times(Plus(m,Times(CN1,pn),C1),Power(x,Plus(m,Times(CN1,C2,pn),C1)),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C2)),Power(Times(Sqr(b),Sqr(pn),Plus(p,C1),Plus(p,C2)),CN1)),Times(Plus(p,C2),Power(Plus(p,C1),CN1),Int(Times(Power(x,m),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C2))),x)),Times(CN1,Plus(m,Times(CN1,pn),C1),Plus(m,Times(CN1,C2,pn),C1),Power(Times(Sqr(b),Sqr(pn),Plus(p,C1),Plus(p,C2)),CN1),Int(Times(Power(x,Plus(m,Times(CN1,C2,pn))),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C2))),x))),And(And(And(And(And(FreeQ(List(a,b),x),IntegersQ(m,pn)),RationalQ(p)),Less(p,CN1)),Unequal(p,CN2)),Less(Less(C0,Times(C2,pn)),Plus(m,C1))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(u_,pn_)))),p_DEFAULT)),x_Symbol),
    Condition(Times(Power(Power(Coefficient(u,x,C1),Plus(m,C1)),CN1),Subst(Int(Times(Power(Plus(x,Times(CN1,Coefficient(u,x,C0))),m),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),p)),x),x,u)),And(And(And(FreeQ(List(a,b,pn,p),x),LinearQ(u,x)),IntegerQ(m)),NonzeroQ(Plus(u,Times(CN1,x)))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(u_,pn_)))),p_DEFAULT)),x_Symbol),
    Condition(Times(Power(Power(Coefficient(u,x,C1),Plus(m,C1)),CN1),Subst(Int(Times(Power(Plus(x,Times(CN1,Coefficient(u,x,C0))),m),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),p)),x),x,u)),And(And(And(FreeQ(List(a,b,pn,p),x),LinearQ(u,x)),IntegerQ(m)),NonzeroQ(Plus(u,Times(CN1,x)))))),
ISetDelayed(Int(Times(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_DEFAULT)))),Power(x_,m_DEFAULT),Power(Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_DEFAULT)))),p_DEFAULT)),x_Symbol),
    Condition(Times(Power(Sinh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C1)),Power(Times(b,pn,Plus(p,C1)),CN1)),And(And(FreeQ(List(a,b,m,pn,p),x),ZeroQ(Plus(m,Times(CN1,pn),C1))),NonzeroQ(Plus(p,C1))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_DEFAULT)))),p_DEFAULT),Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_DEFAULT))))),x_Symbol),
    Condition(Times(Power(Cosh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C1)),Power(Times(b,pn,Plus(p,C1)),CN1)),And(And(FreeQ(List(a,b,m,pn,p),x),ZeroQ(Plus(m,Times(CN1,pn),C1))),NonzeroQ(Plus(p,C1))))),
ISetDelayed(Int(Times(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_DEFAULT)))),Power(x_,m_DEFAULT),Power(Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_DEFAULT)))),p_DEFAULT)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,Times(CN1,pn),C1)),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C1)),Power(Times(b,pn,Plus(p,C1)),CN1)),Times(CN1,Plus(m,Times(CN1,pn),C1),Power(Times(b,pn,Plus(p,C1)),CN1),Int(Times(Power(x,Plus(m,Times(CN1,pn))),Power(Sinh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C1))),x))),And(And(And(FreeQ(List(a,b,p),x),RationalQ(m,pn)),Less(Less(C0,pn),Plus(m,C1))),NonzeroQ(Plus(p,C1))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Cosh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_DEFAULT)))),p_DEFAULT),Sinh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_DEFAULT))))),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,Times(CN1,pn),C1)),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C1)),Power(Times(b,pn,Plus(p,C1)),CN1)),Times(CN1,Plus(m,Times(CN1,pn),C1),Power(Times(b,pn,Plus(p,C1)),CN1),Int(Times(Power(x,Plus(m,Times(CN1,pn))),Power(Cosh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C1))),x))),And(And(And(FreeQ(List(a,b,p),x),RationalQ(m,pn)),Less(Less(C0,pn),Plus(m,C1))),NonzeroQ(Plus(p,C1))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Tanh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_DEFAULT)),x_Symbol),
    Condition(Times(Power(pn,CN1),Subst(Int(Power(Tanh(Plus(a,Times(b,x))),p),x),x,Power(x,pn))),And(FreeQ(List(a,b,m,pn,p),x),ZeroQ(Plus(m,Times(CN1,Plus(pn,Times(CN1,C1)))))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Coth(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_DEFAULT)),x_Symbol),
    Condition(Times(Power(pn,CN1),Subst(Int(Power(Coth(Plus(a,Times(b,x))),p),x),x,Power(x,pn))),And(FreeQ(List(a,b,m,pn,p),x),ZeroQ(Plus(m,Times(CN1,Plus(pn,Times(CN1,C1)))))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Tanh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_)),x_Symbol),
    Condition(Plus(Times(CN1,Power(x,Plus(m,Times(CN1,pn),C1)),Power(Tanh(Plus(a,Times(b,Power(x,pn)))),Plus(p,Times(CN1,C1))),Power(Times(b,pn,Plus(p,Times(CN1,C1))),CN1)),Times(Plus(m,Times(CN1,pn),C1),Power(Times(b,pn,Plus(p,Times(CN1,C1))),CN1),Int(Times(Power(x,Plus(m,Times(CN1,pn))),Power(Tanh(Plus(a,Times(b,Power(x,pn)))),Plus(p,Times(CN1,C1)))),x)),Int(Times(Power(x,m),Power(Tanh(Plus(a,Times(b,Power(x,pn)))),Plus(p,Times(CN1,C2)))),x)),And(And(And(FreeQ(List(a,b),x),RationalQ(m,pn,p)),Greater(p,C1)),Less(Less(C0,pn),Plus(m,C1))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Coth(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_)),x_Symbol),
    Condition(Plus(Times(CN1,Power(x,Plus(m,Times(CN1,pn),C1)),Power(Coth(Plus(a,Times(b,Power(x,pn)))),Plus(p,Times(CN1,C1))),Power(Times(b,pn,Plus(p,Times(CN1,C1))),CN1)),Times(Plus(m,Times(CN1,pn),C1),Power(Times(b,pn,Plus(p,Times(CN1,C1))),CN1),Int(Times(Power(x,Plus(m,Times(CN1,pn))),Power(Coth(Plus(a,Times(b,Power(x,pn)))),Plus(p,Times(CN1,C1)))),x)),Int(Times(Power(x,m),Power(Coth(Plus(a,Times(b,Power(x,pn)))),Plus(p,Times(CN1,C2)))),x)),And(And(And(FreeQ(List(a,b),x),RationalQ(m,pn,p)),Greater(p,C1)),Less(Less(C0,pn),Plus(m,C1))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Tanh(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,Times(CN1,pn),C1)),Power(Tanh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C1)),Power(Times(b,pn,Plus(p,C1)),CN1)),Times(CN1,Plus(m,Times(CN1,pn),C1),Power(Times(b,pn,Plus(p,C1)),CN1),Int(Times(Power(x,Plus(m,Times(CN1,pn))),Power(Tanh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C1))),x)),Int(Times(Power(x,m),Power(Tanh(Plus(a,Times(b,Power(x,pn)))),Plus(p,C2))),x)),And(And(And(FreeQ(List(a,b),x),RationalQ(m,pn,p)),Less(p,CN1)),Less(Less(C0,pn),Plus(m,C1))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Coth(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_)),x_Symbol),
    Condition(Plus(Times(Power(x,Plus(m,Times(CN1,pn),C1)),Power(Coth(Plus(a,Times(b,Power(x,pn)))),Plus(p,C1)),Power(Times(b,pn,Plus(p,C1)),CN1)),Times(CN1,Plus(m,Times(CN1,pn),C1),Power(Times(b,pn,Plus(p,C1)),CN1),Int(Times(Power(x,Plus(m,Times(CN1,pn))),Power(Coth(Plus(a,Times(b,Power(x,pn)))),Plus(p,C1))),x)),Int(Times(Power(x,m),Power(Coth(Plus(a,Times(b,Power(x,pn)))),Plus(p,C2))),x)),And(And(And(FreeQ(List(a,b),x),RationalQ(m,pn,p)),Less(p,CN1)),Less(Less(C0,pn),Plus(m,C1))))),
ISetDelayed(Int(Sech(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),x_Symbol),
    Condition(Times(Power(pn,CN1),Subst(Int(Times(Power(x,Plus(Power(pn,CN1),Times(CN1,C1))),Sech(Plus(a,Times(b,x)))),x),x,Power(x,pn))),And(FreeQ(List(a,b),x),PositiveIntegerQ(Power(pn,CN1))))),
ISetDelayed(Int(Csch(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),x_Symbol),
    Condition(Times(Power(pn,CN1),Subst(Int(Times(Power(x,Plus(Power(pn,CN1),Times(CN1,C1))),Csch(Plus(a,Times(b,x)))),x),x,Power(x,pn))),And(FreeQ(List(a,b),x),PositiveIntegerQ(Power(pn,CN1))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Sech(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_DEFAULT)),x_Symbol),
    Condition(Times(Power(pn,CN1),Subst(Int(Times(Power(x,Plus(Simplify(Times(Plus(m,C1),Power(pn,CN1))),Times(CN1,C1))),Power(Sech(Plus(a,Times(b,x))),p)),x),x,Power(x,pn))),And(FreeQ(List(a,b,m,pn,p),x),PositiveIntegerQ(Simplify(Times(Plus(m,C1),Power(pn,CN1))))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Csch(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_DEFAULT)),x_Symbol),
    Condition(Times(Power(pn,CN1),Subst(Int(Times(Power(x,Plus(Simplify(Times(Plus(m,C1),Power(pn,CN1))),Times(CN1,C1))),Power(Csch(Plus(a,Times(b,x))),p)),x),x,Power(x,pn))),And(FreeQ(List(a,b,m,pn,p),x),PositiveIntegerQ(Simplify(Times(Plus(m,C1),Power(pn,CN1))))))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Sech(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_DEFAULT)),x_Symbol),
    Condition($(Defer($s("Int")),Times(Power(x,m),Power(Sech(Plus(a,Times(b,Power(x,pn)))),p)),x),FreeQ(List(a,b,m,pn,p),x))),
ISetDelayed(Int(Times(Power(x_,m_DEFAULT),Power(Csch(Plus(a_DEFAULT,Times(b_DEFAULT,Power(x_,pn_)))),p_DEFAULT)),x_Symbol),
    Condition($(Defer($s("Int")),Times(Power(x,m),Power(Csch(Plus(a,Times(b,Power(x,pn)))),p)),x),FreeQ(List(a,b,m,pn,p),x)))
  );
}
